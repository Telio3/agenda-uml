# Agenda UML

## Context

This project is a school project for the UML course.
We chose to create a API with NestJS and with a PostgreSQL database.

## How to Install

```bash
# Clone the repository
$ git clone https://gitlab.com/Telio3/agenda-uml

# Go to the repository
$ cd agenda-uml

# Install dependencies
$ pnpm install
```

_If you don't have pnpm installed, you can install it with `npm install -g pnpm`_

---

## How to build

### Development

```bash
# Build the docker image for database
$ docker compose -f docker-compose.dev.yml up

# Run the application
$ pnpm run start:dev
```

The server will be available at [http://localhost:3000](http://localhost:3000)

### Production

```bash
# Build the docker image for database and application
$ docker compose up
```

---

## How to test

```bash
# Run the tests
$ pnpm run test
```
# Class Diagram #

```mermaid

classDiagram

class ContactDetailType{
    <<enumeration>>
    EMAIL
    PHONE
    ADDRESS
    WEBSITE
}

class ContactEntity {
    +id: string
    +name: string
    +agenda: AgendaEntity
    +details: ContactDetailEntity[]
    +constructor(partial: Partial<ContactEntity>)
}

class ContactService {
    <<service>>
    -_contactRepository: Repository<ContactEntity>
    +create(contact: ContactEntity): Promise<ContactEntity>
}

class ContactDetailEntity {
    <<abstract>>
    +id: string
    #_value: string
    +type: number
    +validate(): void
    +constructor(partial: Partial<ContactDetailEntity>)
}


class AddressEntity {
    +validate(): void
    +constructor(partial: Partial<AddressEntity>)
}

class PhoneEntity {
    +validate(): void
    +constructor(partial: Partial<PhoneEntity>)
    }

class EmailEntity {
    +validate(): void
    +constructor(partial: Partial<EmailEntity>)
}

class UserEntity {
    +id: number
    +username: string
    +agendas: AgendaEntity[]
    +constructor(partial: Partial<UserEntity>)
}

class AgendaEntity {
    +id: number
    +name: string
    -owner: UserEntity
    -contacts: ContactEntity[]
    +constructor(partial: Partial<AgendaEntity>)
    +addContact(contact: ContactEntity): void
}

class AgendaService {
    <<service>>
    +create(agenda: AgendaEntity): Promise<AgendaEntity>
    -_agendaRepository: Repository<AgendaEntity>
}
    ContactEntity --o "1..*" ContactDetailEntity : contains
    ContactDetailEntity <|-- PhoneEntity
    ContactDetailEntity <|-- EmailEntity
    ContactDetailEntity <|-- AddressEntity

    ContactDetailType -- PhoneEntity
    ContactDetailType -- EmailEntity
    ContactDetailType -- AddressEntity

    AgendaEntity"1..*" --* UserEntity : has an owner

    AgendaEntity "1" *-- "0..n" ContactEntity : has many contacts

```