import { AgendaEntity } from './agenda/entity/agenda.entity';
import { ContactEntity } from './contact/entity/contact.entity';
import { UserEntity } from './user/entity/user.entity';
import { ContactDetailEntityFactory } from './contact-detail/factory/contact-detail.factory';
import { ContactDetailType } from './contact-detail/enum/contact-detail-type.enum';
import { EmailEntity } from './contact-detail/entities/email.entity';

describe('App', () => {
  describe('script', () => {
    it('should return a new agenda', async () => {
      const user = new UserEntity({
        login: 'test',
      });
      user.password = 'test';

      const agenda = new AgendaEntity({
        name: 'test',
        owner: user,
        contacts: [],
      });

      const contact = new ContactEntity({
        name: 'test',
        agenda,
        contactDetails: [],
      });

      agenda.addContact(contact);

      const mailContactDetail =
        ContactDetailEntityFactory.createContactDetailEntity({
          value: 'test@test.test',
          type: ContactDetailType.EMAIL,
        });

      contact.addDetail(mailContactDetail);

      expect(agenda).toBeDefined();

      expect(agenda.owner).toBeInstanceOf(UserEntity);

      expect(agenda.contacts).toHaveLength(1);

      expect(agenda.contacts[0]).toBeInstanceOf(ContactEntity);

      expect(agenda.contacts[0].contactDetails).toHaveLength(1);

      expect(agenda.contacts[0].contactDetails[0]).toBeInstanceOf(EmailEntity);
    });
  });
});
