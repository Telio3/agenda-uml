import { AgendaEntity } from '../../agenda/entity/agenda.entity';
import { ContactDetailEntity } from '../../contact-detail/abstract-class/contact-detail.abstract-class';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('contacts')
export class ContactEntity {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public name: string;

  @ManyToOne(() => AgendaEntity, (agenda) => agenda.contacts, {
    onDelete: 'CASCADE',
  })
  public agenda: AgendaEntity;

  @OneToMany(
    () => ContactDetailEntity,
    (contactDetail) => contactDetail.contact,
    { cascade: true },
  )
  public contactDetails: ContactDetailEntity[];

  constructor(partial: Partial<ContactEntity>) {
    this.id = partial?.id;
    this.name = partial?.name;
    this.agenda = partial?.agenda;
    this.contactDetails = partial?.contactDetails;
  }

  public addDetail(detail: ContactDetailEntity): void {
    this.contactDetails.push(detail);
  }
}
