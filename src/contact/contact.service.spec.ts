import { Test } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ContactService } from './contact.service';
import { ContactEntity } from './entity/contact.entity';
import { ContactDetailEntityFactory } from '../contact-detail/factory/contact-detail.factory';
import { ContactDetailType } from '../contact-detail/enum/contact-detail-type.enum';

describe('ContactService', () => {
  let contactService: ContactService;
  let contactRepository: Repository<ContactEntity>;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        ContactService,
        {
          provide: getRepositoryToken(ContactEntity),
          useValue: {},
        },
      ],
    }).compile();

    contactService = moduleRef.get<ContactService>(ContactService);
    contactRepository = moduleRef.get(getRepositoryToken(ContactEntity));
  });

  describe('create', () => {
    it('should return a new contact', async () => {
      const contact = new ContactEntity({
        name: 'test',
        contactDetails: [],
      });

      contact.addDetail(
        ContactDetailEntityFactory.createContactDetailEntity({
          value: 'test@test.test',
          type: ContactDetailType.EMAIL,
        }),
      );

      contactRepository.save = jest.fn().mockResolvedValue(contact);

      expect(await contactService.create(contact)).toEqual(contact);
    });
  });
});
