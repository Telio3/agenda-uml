import { Injectable } from '@nestjs/common';
import { ContactEntity } from './entity/contact.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class ContactService {
  constructor(
    @InjectRepository(ContactEntity)
    private readonly _contactRepository: Repository<ContactEntity>,
  ) {}

  public async create(contact: ContactEntity): Promise<ContactEntity> {
    return await this._contactRepository.save(contact);
  }
}
