import { Module } from '@nestjs/common';
import { ContactService } from './contact.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ContactEntity } from './entity/contact.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ContactEntity])],
  exports: [TypeOrmModule, ContactService],
  providers: [ContactService],
})
export class ContactModule {}
