import { Body, Controller, Post } from '@nestjs/common';
import { UserEntity } from './entity/user.entity';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('user')
@Controller('user')
export class UserController {
  constructor(private readonly _userService: UserService) {}

  @Post()
  public create(@Body() user: CreateUserDto): Promise<UserEntity> {
    return this._userService.create(new UserEntity(user));
  }
}
