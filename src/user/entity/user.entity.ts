import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BeforeInsert,
  OneToMany,
} from 'typeorm';
import * as bcrypt from 'bcrypt';
import { Exclude } from 'class-transformer';
import { AgendaEntity } from '../../agenda/entity/agenda.entity';

@Entity('users')
export class UserEntity {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ unique: true })
  public login: string;

  @Exclude()
  @Column({ name: 'password' })
  private _password: string;

  public set password(password: string) {
    this._password = password;
  }

  @BeforeInsert()
  public async hashPassword(): Promise<void> {
    this._password = await bcrypt.hash(this._password, 10);
  }

  public comparePassword(password: string): Promise<boolean> {
    return bcrypt.compare(password, this._password);
  }

  @OneToMany(() => AgendaEntity, (agenda) => agenda.owner)
  public agendas: AgendaEntity[];

  constructor(partial: Partial<UserEntity>) {
    this.id = partial?.id;
    this.login = partial?.login;
    this._password = partial?.password;
    this.agendas = partial?.agendas;
  }
}
