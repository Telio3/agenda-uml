import { Test } from '@nestjs/testing';
import { UserService } from './user.service';
import { UserEntity } from './entity/user.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

describe('UserService', () => {
  let userService: UserService;
  let userRepository: Repository<UserEntity>;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        UserService,
        {
          provide: getRepositoryToken(UserEntity),
          useValue: {},
        },
      ],
    }).compile();

    userService = moduleRef.get<UserService>(UserService);
    userRepository = moduleRef.get(getRepositoryToken(UserEntity));
  });

  describe('create', () => {
    it('should return a new user', async () => {
      const user = new UserEntity({
        login: 'test',
      });
      user.password = 'test';

      userRepository.save = jest.fn().mockResolvedValue(user);

      expect(await userService.create(user)).toEqual(user);
    });
  });
});
