import { Injectable } from '@nestjs/common';
import { AgendaEntity } from './entity/agenda.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class AgendaService {
  constructor(
    @InjectRepository(AgendaEntity)
    private readonly _agendaRepository: Repository<AgendaEntity>,
  ) {}

  public async create(agenda: AgendaEntity): Promise<AgendaEntity> {
    return await this._agendaRepository.save(agenda);
  }
}
