import { Module } from '@nestjs/common';
import { AgendaService } from './agenda.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AgendaEntity } from './entity/agenda.entity';

@Module({
  imports: [TypeOrmModule.forFeature([AgendaEntity])],
  exports: [TypeOrmModule, AgendaService],
  providers: [AgendaService],
})
export class AgendaModule {}
