import { ContactEntity } from '../../contact/entity/contact.entity';
import { UserEntity } from '../../user/entity/user.entity';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('agendas')
export class AgendaEntity {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public name: string;

  @ManyToOne(() => UserEntity, (user) => user.agendas)
  public owner: UserEntity;

  @OneToMany(() => ContactEntity, (contact) => contact.agenda, {
    cascade: true,
  })
  public contacts: ContactEntity[];

  constructor(partial: Partial<AgendaEntity>) {
    this.id = partial?.id;
    this.name = partial?.name;
    this.owner = partial?.owner;
    this.contacts = partial?.contacts;
  }

  public addContact(contact: ContactEntity): void {
    this.contacts.push(contact);
  }
}
