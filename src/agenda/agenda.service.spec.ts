import { Test } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AgendaService } from './agenda.service';
import { AgendaEntity } from './entity/agenda.entity';
import { UserEntity } from '../user/entity/user.entity';
import { UserService } from '../user/user.service';
import { ContactEntity } from '../contact/entity/contact.entity';

describe('AgendaService', () => {
  let agendaService: AgendaService;
  let agendaRepository: Repository<AgendaEntity>;
  let userService: UserService;
  let userRepository: Repository<UserEntity>;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        AgendaService,
        {
          provide: getRepositoryToken(AgendaEntity),
          useValue: {},
        },
        UserService,
        {
          provide: getRepositoryToken(UserEntity),
          useValue: {},
        },
      ],
    }).compile();

    agendaService = moduleRef.get<AgendaService>(AgendaService);
    agendaRepository = moduleRef.get(getRepositoryToken(AgendaEntity));
    userService = moduleRef.get<UserService>(UserService);
    userRepository = moduleRef.get(getRepositoryToken(UserEntity));
  });

  describe('create', () => {
    it('should return a new agenda', async () => {
      const user = new UserEntity({
        login: 'test',
      });
      user.password = 'test';

      userRepository.save = jest.fn().mockResolvedValue(user);

      expect(await userService.create(user)).toEqual(user);

      const agenda = new AgendaEntity({
        name: 'test',
        owner: user,
        contacts: [],
      });

      agenda.addContact(
        new ContactEntity({
          name: 'test',
          agenda,
        }),
      );

      agendaRepository.save = jest.fn().mockResolvedValue(agenda);

      const createdAgenda = await agendaService.create(agenda);

      expect(createdAgenda).toEqual(agenda);
    });
  });
});
