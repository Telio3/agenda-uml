import { Module } from '@nestjs/common';
import { DatabaseModule } from './database/database.module';
import { ContactModule } from './contact/contact.module';
import { UserModule } from './user/user.module';
import { AgendaModule } from './agenda/agenda.module';
import { ContactDetailModule } from './contact-detail/contact-detail.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    DatabaseModule,
    ContactModule,
    UserModule,
    ContactDetailModule,
    AgendaModule,
    AuthModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
