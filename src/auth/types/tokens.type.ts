import { UserEntity } from '../../user/entity/user.entity';

export type Tokens = {
  accessToken: string;
  user: UserEntity;
};
