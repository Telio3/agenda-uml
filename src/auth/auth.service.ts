import { ForbiddenException, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { UserService } from '../user/user.service';
import { LoginDto } from './dto/login.dto';
import { JwtPayload, Tokens } from './types';

@Injectable()
export class AuthService {
  constructor(
    private readonly _userService: UserService,
    private readonly _jwtService: JwtService,
    private readonly _configService: ConfigService,
  ) {}

  public async login(loginDto: LoginDto): Promise<Tokens> {
    const user = await this._userService.findOneByLogin(loginDto.login);

    if (!user) throw new ForbiddenException('Access Denied');

    const isPasswordValid = await user.comparePassword(loginDto.password);

    if (!isPasswordValid) throw new ForbiddenException('Access Denied');

    return {
      accessToken: await this.getTokens(user.id),
      user,
    };
  }

  public async getTokens(userId: number): Promise<string> {
    const jwtPayload: JwtPayload = {
      sub: userId,
    };

    const accessToken = await this._jwtService.signAsync(jwtPayload, {
      secret: this._configService.get<string>('JWT_KEY'),
    });

    return accessToken;
  }
}
