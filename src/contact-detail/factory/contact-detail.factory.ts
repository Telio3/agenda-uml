import { ContactDetailEntity } from '../abstract-class/contact-detail.abstract-class';
import { AddressEntity } from '../entities/address.entity';
import { EmailEntity } from '../entities/email.entity';
import { PhoneEntity } from '../entities/phone.entity';
import { WebsiteEntity } from '../entities/website.entity';
import { ContactDetailType } from '../enum/contact-detail-type.enum';

export class ContactDetailEntityFactory {
  public static createContactDetailEntity(contactDetail: {
    value: string;
    type: ContactDetailType;
  }): ContactDetailEntity {
    switch (contactDetail.type) {
      case ContactDetailType.EMAIL:
        return new EmailEntity(contactDetail);
      case ContactDetailType.PHONE:
        return new PhoneEntity(contactDetail);
      case ContactDetailType.ADDRESS:
        return new AddressEntity(contactDetail);
      case ContactDetailType.WEBSITE:
        return new WebsiteEntity(contactDetail);
      default:
        throw new Error('Contact detail type not found');
    }
  }
}
