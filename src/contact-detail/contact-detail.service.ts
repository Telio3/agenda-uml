import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ContactDetailEntity } from './abstract-class/contact-detail.abstract-class';

@Injectable()
export class ContactDetailService {
  constructor(
    @InjectRepository(ContactDetailEntity)
    private readonly _contactDetailRepository: Repository<ContactDetailEntity>,
  ) {}

  public async create(
    contactDetail: ContactDetailEntity,
  ): Promise<ContactDetailEntity> {
    return await this._contactDetailRepository.save(contactDetail);
  }
}
