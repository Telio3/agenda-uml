export enum ContactDetailType {
  EMAIL,
  PHONE,
  ADDRESS,
  WEBSITE,
}
