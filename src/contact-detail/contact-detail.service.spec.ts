import { Test } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ContactDetailService } from './contact-detail.service';
import { ContactDetailEntity } from './abstract-class/contact-detail.abstract-class';
import { ContactDetailType } from './enum/contact-detail-type.enum';
import { ContactDetailEntityFactory } from './factory/contact-detail.factory';
import { EmailEntity } from './entities/email.entity';
import { PhoneEntity } from './entities/phone.entity';
import { AddressEntity } from './entities/address.entity';
import { WebsiteEntity } from './entities/website.entity';

describe('ContactDetailService', () => {
  let contactDetailService: ContactDetailService;
  let contactDetailRepository: Repository<ContactDetailEntity>;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        ContactDetailService,
        {
          provide: getRepositoryToken(ContactDetailEntity),
          useValue: {},
        },
      ],
    }).compile();

    contactDetailService =
      moduleRef.get<ContactDetailService>(ContactDetailService);
    contactDetailRepository = moduleRef.get(
      getRepositoryToken(ContactDetailEntity),
    );
  });

  describe('create', () => {
    it('should return a many contact details', async () => {
      const createdEmailContactDetail =
        ContactDetailEntityFactory.createContactDetailEntity({
          value: 'test@test.test',
          type: ContactDetailType.EMAIL,
        });

      expect(createdEmailContactDetail).toBeInstanceOf(EmailEntity);

      contactDetailRepository.save = jest
        .fn()
        .mockResolvedValue(createdEmailContactDetail);

      expect(
        await contactDetailService.create(createdEmailContactDetail),
      ).toEqual(createdEmailContactDetail);

      expect(() => {
        ContactDetailEntityFactory.createContactDetailEntity({
          value: 'test.test',
          type: ContactDetailType.EMAIL,
        });
      }).toThrow('Value is not a valid email');

      const createdPhoneContactDetail =
        ContactDetailEntityFactory.createContactDetailEntity({
          value: '0276892373',
          type: ContactDetailType.PHONE,
        });

      expect(createdPhoneContactDetail).toBeInstanceOf(PhoneEntity);

      contactDetailRepository.save = jest
        .fn()
        .mockResolvedValue(createdPhoneContactDetail);

      expect(
        await contactDetailService.create(createdPhoneContactDetail),
      ).toEqual(createdPhoneContactDetail);

      expect(() => {
        ContactDetailEntityFactory.createContactDetailEntity({
          value: '829E183E0320',
          type: ContactDetailType.PHONE,
        });
      }).toThrow('Value is not a valid phone number');

      const createdAddressContactDetail =
        ContactDetailEntityFactory.createContactDetailEntity({
          value: '2 Rue du Faubourg Saint-Antoine, 75012 Paris',
          type: ContactDetailType.ADDRESS,
        });

      expect(createdAddressContactDetail).toBeInstanceOf(AddressEntity);

      contactDetailRepository.save = jest
        .fn()
        .mockResolvedValue(createdAddressContactDetail);

      expect(
        await contactDetailService.create(createdAddressContactDetail),
      ).toEqual(createdAddressContactDetail);

      expect(() => {
        ContactDetailEntityFactory.createContactDetailEntity({
          value: 'rue des Lilas, 75020 Paris',
          type: ContactDetailType.ADDRESS,
        });
      }).toThrow('Value is not a valid address');

      const createdWebsiteContactDetail =
        ContactDetailEntityFactory.createContactDetailEntity({
          value: 'https://www.google.com',
          type: ContactDetailType.WEBSITE,
        });

      expect(createdWebsiteContactDetail).toBeInstanceOf(WebsiteEntity);

      contactDetailRepository.save = jest
        .fn()
        .mockResolvedValue(createdWebsiteContactDetail);

      expect(
        await contactDetailService.create(createdWebsiteContactDetail),
      ).toEqual(createdWebsiteContactDetail);

      expect(() => {
        ContactDetailEntityFactory.createContactDetailEntity({
          value: 'test',
          type: ContactDetailType.WEBSITE,
        });
      }).toThrow('Value is not a valid website');
    });
  });
});
