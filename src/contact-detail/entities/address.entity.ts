import { BeforeInsert, BeforeUpdate } from 'typeorm';
import { ContactDetailEntity } from '../abstract-class/contact-detail.abstract-class';
import { ContactDetailType } from '../enum/contact-detail-type.enum';

export class AddressEntity extends ContactDetailEntity {
  @BeforeInsert()
  @BeforeUpdate()
  public validate(): void {
    if (!this.value) {
      throw new Error('Value is required');
    }

    if (
      !this.value.match(
        /^[0-9]{1,4}\s[A-Za-zÀ-ÖØ-öø-ÿ\s'-]{3,},?\s?\d{5}\s[A-Za-zÀ-ÖØ-öø-ÿ\s'-]{2,}$/,
      )
    ) {
      throw new Error('Value is not a valid address');
    }
  }

  constructor(partial: Partial<AddressEntity>) {
    super();

    this.id = partial?.id;
    this.value = partial?.value;
    this.validate();
    this.type = ContactDetailType.ADDRESS;
    this.contact = partial?.contact;
  }
}
