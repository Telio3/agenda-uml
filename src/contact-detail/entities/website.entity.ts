import { BeforeInsert, BeforeUpdate } from 'typeorm';
import { ContactDetailEntity } from '../abstract-class/contact-detail.abstract-class';
import { ContactDetailType } from '../enum/contact-detail-type.enum';

export class WebsiteEntity extends ContactDetailEntity {
  @BeforeInsert()
  @BeforeUpdate()
  public validate(): void {
    if (!this.value) {
      throw new Error('Value is required');
    }

    if (
      !this.value.match(
        /^(https?:\/\/)?([A-Za-z0-9-]+\.)+[A-Za-z]{2,}(:[0-9]+)?(\/.*)?$/,
      )
    ) {
      throw new Error('Value is not a valid website');
    }
  }

  constructor(partial: Partial<WebsiteEntity>) {
    super();

    this.id = partial?.id;
    this.value = partial?.value;
    this.validate();
    this.type = ContactDetailType.WEBSITE;
    this.contact = partial?.contact;
  }
}
