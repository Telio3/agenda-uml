import { BeforeInsert, BeforeUpdate } from 'typeorm';
import { ContactDetailEntity } from '../abstract-class/contact-detail.abstract-class';
import { ContactDetailType } from '../enum/contact-detail-type.enum';

export class EmailEntity extends ContactDetailEntity {
  @BeforeInsert()
  @BeforeUpdate()
  public validate(): void {
    if (!this.value) {
      throw new Error('Value is required');
    }

    if (!this.value.match(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/)) {
      throw new Error('Value is not a valid email');
    }
  }

  constructor(partial: Partial<EmailEntity>) {
    super();

    this.id = partial?.id;
    this.value = partial?.value;
    this.validate();
    this.type = ContactDetailType.EMAIL;
    this.contact = partial?.contact;

    this.validate = this.validate.bind(this);
  }
}
