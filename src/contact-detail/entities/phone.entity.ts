import { BeforeInsert, BeforeUpdate } from 'typeorm';
import { ContactDetailEntity } from '../abstract-class/contact-detail.abstract-class';
import { ContactDetailType } from '../enum/contact-detail-type.enum';

export class PhoneEntity extends ContactDetailEntity {
  @BeforeInsert()
  @BeforeUpdate()
  public validate(): void {
    if (!this.value) {
      throw new Error('Value is required');
    }

    if (!this.value.match(/^(0|\+33)[1-9]([-. ]?[0-9]{2}){4}$/)) {
      throw new Error('Value is not a valid phone number');
    }
  }

  constructor(partial: Partial<PhoneEntity>) {
    super();

    this.id = partial?.id;
    this.value = partial?.value;
    this.validate();
    this.type = ContactDetailType.PHONE;
    this.contact = partial?.contact;
  }
}
