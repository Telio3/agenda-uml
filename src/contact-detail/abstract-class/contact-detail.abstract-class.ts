import { ContactEntity } from '../../contact/entity/contact.entity';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { ContactDetailType } from '../enum/contact-detail-type.enum';

@Entity('contactDetails')
export abstract class ContactDetailEntity {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ name: 'value' })
  protected _value: string;

  public get value(): string {
    return this._value;
  }

  public set value(value: string) {
    this._value = value;
  }

  @Column({
    type: 'enum',
    enum: ContactDetailType,
  })
  public type: number;

  @BeforeInsert()
  @BeforeUpdate()
  public validate(): void {
    if (!this.value) {
      throw new Error('Value is required');
    }
  }

  @ManyToOne(() => ContactEntity, (contact) => contact.contactDetails, {
    onDelete: 'CASCADE',
  })
  public contact: ContactEntity;
}
