import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ContactDetailEntity } from './abstract-class/contact-detail.abstract-class';

@Module({
  imports: [TypeOrmModule.forFeature([ContactDetailEntity])],
  exports: [TypeOrmModule],
  controllers: [],
  providers: [],
})
export class ContactDetailModule {}
