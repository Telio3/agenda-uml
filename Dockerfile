FROM node:18

RUN npm install -g pnpm

WORKDIR /usr/src/app

COPY pnpm-lock.yaml .
COPY package*.json ./
COPY tsconfig*.json ./

RUN pnpm install

COPY . .

RUN pnpm run build

CMD ["pnpm", "run", "start:prod"]
